<?php

include_once('password.php');

class Database
{
    private $db;

    //------------------------
    //			CONNECT
    //------------------------

    public function __construct($databaseFilename)
    {
        $this->db = new PDO($databaseFilename);
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING); // TO BE REMOVED
    }

    //------------------------
    //			USER
    //------------------------

    public function addUser($username, $firstName, $lastName, $birthDate, $email, $password)
    {
        $options = array('cost' => 12);

        $stmt = $this->db->prepare('INSERT INTO user VALUES(?, ?, ?, ?, ?, ?, ?)');
        $stmt->execute(array(
                NULL,
                $username,
                $firstName,
                $lastName,
                $birthDate,
                $email,
                password_hash($password, PASSWORD_DEFAULT, $options))
        );
    }

    public function getUsername($id)
    {
        $stmt = $this->db->prepare('SELECT username,first_name,last_name FROM user WHERE user_id = ?');
        $stmt->execute(array($id));

        $row = $stmt->fetch();

        if (isset($row['first_name']) && isset($row['last_name']))
        {
            return $row['first_name'] . " " . $row['last_name'];
        } else {
            return $row['username'];
        }
    }

    public function getUserID($username)
    {
        $stmt = $this->db->prepare('SELECT user_id FROM user WHERE username = ?');
        $stmt->execute(array($username));

        $id = $stmt->fetch();

        return $id['user_id'];
    }

    public function verifyUsernameExistence($username)
    {
        $userSelect = $this->db->prepare('SELECT username FROM user WHERE username = ?');
        $userSelect->execute(array($username));
        $userResult = $userSelect->fetch();

        if($userResult !== false)
            return true;

        return false;
    }

    public function verifyEmailExistence($email)
    {
        $userSelect = $this->db->prepare('SELECT email FROM user WHERE email = ?');
        $userSelect->execute(array($email));
        $userResult = $userSelect->fetch();

        if($userResult !== false)
            return true;

        return false;
    }

    public function verifyUserPassword($username, $password)
    {
        $passwordSelect = $this->db->prepare('SELECT password FROM user WHERE username = ?');
        $passwordSelect->execute(array($username));
        $passwordResult = $passwordSelect->fetch();

        if($passwordResult !== false && password_verify($password, $passwordResult['password']))
            return true;

        return false;
    }


    //------------------------
    //			POLL
    //------------------------

    public function addPoll($user_id, $poll, $public)
    {
        $stmt = $this->db->prepare('INSERT INTO poll VALUES(?,?,?,?)');
        $result = $stmt->execute(array(NULL, $user_id, $poll, $public));

        return $this->db->lastInsertId();
    }

    public function addQuestion($question, $poll_id, $array_of_answers)
    {
        $stmt = $this->db->prepare('INSERT INTO question VALUES(?,?,?)');
        $result = $stmt->execute(array(NULL, $poll_id, $question));

        $question_id = $this->db->lastInsertId();
        $count = count($array_of_answers);
        $keys = array_keys($array_of_answers);

        for($i = 0; $i < $count; $i++)
            $this->addAnswer($array_of_answers[$keys[$i]], $question_id);
    }

    public function addAnswer($answer, $question_id)
    {
        $stmt = $this->db->prepare('INSERT INTO answer VALUES(?,?,?)');
        $result = $stmt->execute(array(NULL, $question_id, $answer));
    }

    public function showPolls()
    {
        $stmt = $this->db->prepare('SELECT * FROM poll WHERE public = 1');
        $stmt->execute();

        $result = $stmt->fetchAll();

        return $result;
    }



    public function verifyPollExistence($pollName)
    {
        $stmt = $this->db->prepare('SELECT string FROM poll WHERE string = ?');
        $result = $stmt->execute(array($pollName));

        if($result !== NULL)
            return true;

        return false;
    }

    public function vote($question_id, $answer_id, $user_id)
    {
        $stmt = $this->db->prepare('INSERT INTO voting_history VALUES(?, ?, ?, ?)');
        $stmt->execute(array(NULL, $question_id, $answer_id, $user_id));
    }

    public function hasUserVoted($poll_id, $user_id)
    {
        $stmt = $this->db->prepare('SELECT question_id FROM question WHERE poll_id = ?');
        $stmt->execute(array($poll_id));
        $question_id = $stmt->fetch();


        $stmt = $this->db->prepare('SELECT user_id FROM voting_history WHERE question_id = ?');
        $stmt->execute(array($question_id['question_id']));
        $result = $stmt->fetchAll();

        return (in_array_r($user_id,$result));
    }

    public function showQuestion($question_id)
    {
        $stmt = $this->db->prepare('SELECT * FROM answer WHERE question_id = ?');
        $stmt->execute(array($question_id));

        $answers = $stmt->fetchAll();

        $stmt = $this->db->prepare('SELECT * FROM question WHERE question_id = ?');
        $stmt->execute(array($question_id));

        $question = $stmt->fetch();

        return array('question' => $question, 'answers' => $answers);

    }

    public function showQuestions($poll_id)
    {
        /* Retrieves the id of all the questions from the poll */
        $stmt = $this->db->prepare('SELECT question_id FROM question WHERE poll_id = ?');
        $stmt->execute(array($poll_id));

        $result = $stmt->fetchAll();

        $q_array = array();

        /* For each question retrieves all answers */
        foreach ($result as $question_id) {
            array_push($q_array, $this->showQuestion($question_id['question_id']));
        }

        /* Returns array with format [ans1, ans2, ans3] */
        return $q_array;

    }

    public function showPoll($poll_id)
    {
        $stmt = $this->db->prepare('SELECT * FROM poll WHERE poll_id = ?');
        $stmt->execute(array($poll_id));

        $result = $stmt->fetchAll();

        return $result;
    }

    public function getQuestion($answer_id)
    {
        $stmt = $this->db->prepare('SELECT question_id FROM answer WHERE answer_id = ? ');
        $stmt->execute(array($answer_id));

        $result = $stmt->fetch();

        return $result;
    }

    public function searchPolls($search)
    {
        $stmt = $this->db->prepare("SELECT * FROM poll WHERE string LIKE '%{$search}%' ORDER BY poll_id DESC");
        $stmt->execute();

        $result = $stmt->fetchAll();

        return $result;

    }

    public function searchOwnPolls($search)
    {
        $stmt = $this->db->prepare("SELECT * FROM poll WHERE user_id = ? AND string LIKE '%{$search}%' ORDER BY poll_id DESC");
        $stmt->execute(array($_SESSION['userID']));

        $result = $stmt->fetchAll();

        return $result;

    }

    public function getQuestionsIDs($poll_id)
    {
        $stmt = $this->db->prepare("SELECT question_id, string FROM question WHERE poll_id=?");
        $stmt->execute(array($poll_id));

        $result = $stmt->fetchAll();

        return $result;
    }

    public function getAnswers($question_id)
    {
        $stmt = $this->db->prepare("SELECT answer_id, string FROM answer WHERE question_id=?");

        $stmt->execute(array($question_id));

        $result = $stmt->fetchAll();

        return $result;
    }

    public function getAnswersCount($question_id, $answer_id)
    {
        $stmt = $this->db->prepare("SELECT COUNT(*) AS \"count\" FROM voting_history WHERE question_id=? AND answer_id=? GROUP BY answer_id");

        $stmt->execute(array($question_id, $answer_id));

        $result = $stmt->fetchAll();

        return $result;
    }
}
?>