. mode columns
. headers on
. nullvalue NULL

DROP TABLE if exists answer;
DROP TABLE if exists question;
DROP TABLE if exists user;
DROP TABLE if exists user_poll;
DROP TABLE if exists poll;
DROP TABLE if exists voting_history;

CREATE TABLE user
(
	user_id INTEGER PRIMARY KEY AUTOINCREMENT,
	username TEXT NOT NULL,
	first_name TEXT,
	last_name TEXT,
	date_of_birth TEXT NOT NULL,
	email TEXT NOT NULL,
	password TEXT NOT NULL
);

create TABLE poll
(
	poll_id INTEGER PRIMARY KEY AUTOINCREMENT ,
	user_id INTEGER REFERENCES user(user_id),
	string TEXT NOT NULL,
	public BOOLEAN
);

CREATE TABLE voting_history
(
	voting_history_id INTEGER PRIMARY KEY AUTOINCREMENT,
	question_id INTEGER REFERENCES question(question_id),
	answer_id INTEGER REFERENCES answer(answer_id),
	user_id INTEGER REFERENCES user(user_id)
);

create TABLE question
(
	question_id INTEGER PRIMARY KEY AUTOINCREMENT,
	poll_id INT REFERENCES poll(poll_id),
	string TEXT NOT NULL
);

create TABLE answer
(
	answer_id INTEGER PRIMARY KEY AUTOINCREMENT,
	question_id INT REFERENCES question(question_id),
	string TEXT NOT NULL
);