$(document).ready(function() {

    function search() {
        $.ajax({
            type: "GET",
            url: "../jquery/search.php",
            data: 'search=' + $('#search_input').val(),
            success: function(msg) {
                $('#poll_list').html(msg);
            }
        });
    }

    function searchOwn() {
        $.ajax({
            type: "GET",
            url: "../jquery/search.php",
            data: 'search=' + $('#search_input').val(),
            success: function(msg) {
                $('#poll_list').html(msg);
            }
        });
    }

    $('#search_input').keyup(search); //Change
    search();
}); //Document Ready