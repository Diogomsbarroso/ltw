<?php
include_once("../init.php");

$pollID = $_POST["id"];
$questionsIDs = $dataBase->getQuestionsIDs($pollID);

$result = [];
$size = count($questionsIDs);
for($i = 0; $i < $size; $i++)
{
    $element = $questionsIDs[$i];
    $result[$i][0] = $element["question_id"];
    $result[$i][1] = array("question_name" => $element["string"]);

    $answers = $dataBase->getAnswers($result[$i][0]);
    $answersSize = count($answers);
    $answersResult = [];
    for($j = 0; $j < $answersSize; $j++)
    {
        $array = $answers[$j];
        $answerID = $array["answer_id"];

        $answersCount = $dataBase->getAnswersCount($pollID, $answerID);
        $answersResult[$j]   = array(
            "answer_name" => $array["string"],
            "count" => isset($answersCount[0]) ? intval($answersCount[0]["count"]) : 0
        );
    }
    $result[$i][2] = $answersResult;
}

$response = json_encode(array_values($result));

echo $response;
?>