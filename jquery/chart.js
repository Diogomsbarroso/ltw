function showChart(poll_id)
{
    var request = new XMLHttpRequest();

    request.onreadystatechange = function()
    {
        if(request.readyState == 4 && request.status == 200)
        {
            createChart($.parseJSON(request.responseText));
        }
    }

    request.open("POST", "../jquery/chart.php", true);
    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    request.send("id=" + poll_id);
}

function createChart(json)
{
    for(var id in json)
    {
        var plotTitle = json[id][1].question_name;
        var answers = json[id][2];
        var answersList = new Array();
        for(var j in answers)
        {
            answersList[j] = new Array(2);
            answersList[j][1] = answers[j].answer_name;
            answersList[j][0] = answers[j].count;
        }

        var h = answersList.length * 100;

        var chartName = "chart" + id;
        $("#charts").append("<div id=\"" + chartName + "\" style=\"height:" + h + "px;\"></div>");

        $.jqplot(chartName, [answersList], {
            title: plotTitle,
            series: [{
                renderer: $.jqplot.BarRenderer,
                rendererOptions: {
                    barDirection: 'horizontal'
                }
            }],
            axesDefaults: {
                tickRenderer: $.jqplot.CanvasAxisTickRenderer,
                tickOptions: {
                    fontSize: '1em'
                }
            },
            axes: {
                yaxis: {
                    renderer: $.jqplot.CategoryAxisRenderer
                }
            },
            seriesColors: ['#70bf44']
        });
    }
}