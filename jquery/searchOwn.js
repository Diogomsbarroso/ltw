$(document).ready(function() {

    function searchOwn() {
        $.ajax({
            type: "GET",
            url: "../jquery/searchOwn.php",
            data: 'search=' + $('#search_input').val(),
            success: function(msg) {
                $('#poll_list').html(msg);
            }
        });
    }

    $('#search_input').keyup(searchOwn); //Change
    searchOwn();
}); //Document Ready