<?php
include_once("../init.php");

if(!isLoggedIn())
    header("location: " . $_BASE_URL);

$pollName = getPostVariable("pollName");
$isPublic = getPostVariable("isPublic");
$imageTitle = getPostVariable("imageTitle");

$allowedTypes = array('image/jpeg');
$fileType = $_FILES['image']['type'];

if(!in_array($fileType, $allowedTypes))
{
    $message = "You may only upload jpeg images!";
}
else if($imageTitle === "" || $pollName === "" ||  $isPublic === "")
{
    $message = "";
    if($pollName === "")
        $message .= "Poll name not valid!\n";
    if($isPublic === "")
        $isPublic .= "Public field not valid!\n";
    if($imageTitle === "")
        $message .= "Image's title not valid!\n";
}
else
{
    // Add poll to database:
    $pollID = $dataBase->addPoll($_SESSION["userID"], $pollName, $isPublic);

    // Create filenames:
    $originalFilename = $_BASE_DIR . "images/" . $pollID . ".jpg";
    $thumbnailFilename = $_BASE_DIR . "images/" . $pollID . "_tb.jpg";

    move_uploaded_file($_FILES['image']['tmp_name'], $originalFilename);
    $original = imagecreatefromjpeg($originalFilename);

    // Get width, height and the minimum of the two:
    $width = imagesx($original);
    $height = imagesy($original);
    $square = min($width, $height);

    // Create small square thumbnail:
    $thumbnailSize = 150;
    $thumbnail = imagecreatetruecolor($thumbnailSize, $thumbnailSize);
    imagecopyresized($thumbnail, $original, 0, 0, ($width>$square)?($width-$square)/2:0, ($height>$square)?($height-$square)/2:0, $thumbnailSize, $thumbnailSize, $square, $square);
    imagejpeg($thumbnail, $thumbnailFilename);

    $message = "Poll successfully created!";
}

$_SESSION["message"] = $message;
header("Location: createPollView.php");
?>