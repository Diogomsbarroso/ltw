<?php
include_once('../init.php');
include_once($_BASE_DIR . 'templates/header.php');
?>
    <form action="createPollAction.php" method="post" enctype="multipart/form-data">
        <input type="text" name="pollName" placeholder="Poll Name"><br>
        <label for="isPublic">Is Public</label>
        <input id="isPublic" type="checkbox" name="isPublic"><br>
        <input type="text" name="imageTitle" placeholder="Image Title"><br>
        <input type="file" name="image"><br>
        <input type="submit" value="Create Poll"><br>
    </form>
    <? if (isset($_SESSION['message'])) { ?>
        <span class="error"><?=$_SESSION['message']?></span>
    <? unset($_SESSION['message']); ?>
    <? } ?>
<?php
include_once($_BASE_DIR . 'templates/footer.php');
?>
