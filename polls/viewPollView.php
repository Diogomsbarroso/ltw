<?php
include_once('../init.php');
include_once($_BASE_DIR . 'templates/header.php');
?>

<?php
if (!$dataBase->hasUserVoted($_GET['id'],$_SESSION['userID']))
{

    header("Location: votePollView.php?id=" . $_GET['id']);
    exit();

}
$poll = $dataBase->showPoll($_GET['id']);

$questions = $dataBase->showQuestions($_GET['id']);
?>

    <h1><?= $poll[0]['string'] ?></h1>

    <div id="charts"></div>

    <script language="javascript" type="text/javascript" src="../jqplot/jquery.min.js"></script>
    <script language="javascript" type="text/javascript" src="../jqplot/jquery.jqplot.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../jqplot/jquery.jqplot.css" />
    <script type="text/javascript" src="<?= $_BASE_URL?>jqplot/plugins/jqplot.dateAxisRenderer.min.js"></script>
    <script type="text/javascript" src="<?= $_BASE_URL?>jqplot/plugins/jqplot.canvasTextRenderer.min.js"></script>
    <script type="text/javascript" src="<?= $_BASE_URL?>jqplot/plugins/jqplot.canvasAxisTickRenderer.min.js"></script>
    <script type="text/javascript" src="<?= $_BASE_URL?>jqplot/plugins/jqplot.categoryAxisRenderer.min.js"></script>
    <script type="text/javascript" src="<?= $_BASE_URL?>jqplot/plugins/jqplot.barRenderer.min.js"></script>
    <script language="javascript" type="text/javascript" src="../jquery/chart.js"></script>
    <script type="text/javascript">showChart(<?=$_GET["id"]?>);</script>

<?php
include_once($_BASE_DIR . 'templates/footer.php');
?>