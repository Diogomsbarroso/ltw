<?php

include_once("../init.php");

foreach ($_POST as $answer) {
    $question_id = $dataBase->getQuestion($answer)['question_id'];

    $dataBase->vote($question_id, $answer, $_SESSION['userID']);
}

header("Location: " . $_SERVER['HTTP_REFERER']);
?>