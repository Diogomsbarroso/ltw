<?php
include_once("../init.php");

if(!isLoggedIn())
    header("location: ../home/homeView.php");
?>
<!DOCTYPE HTML>
<html>
<head>
    <title>Edit Polls</title>
    <meta charset="UTF-8">
    <script type="text/javascript" src="addQuestionsAction.js"></script>
</head>
<body>
<?include_once($_BASE_DIR . "templates/navigationBar.php"); ?>


<section>


    <?php

    $poll = $dataBase->showPoll($_GET['id']);
    $pollId = $_GET['id'];

    $questions = $dataBase->showQuestions($_GET['id']);
    
        /*
        var_dump($questions);
    */

    $questions = array_filter($questions);

    if(!empty($questions)) {
        ?>

        <h2>
            <?= $poll[0]['string'] ?>
        </h2>

        <form name="addQuestions" action="addQuestionsAction.php" method="post">
            <?php
            foreach ($questions as $question) {
                /*var_dump($question);*/
                ?>
                <div class = "question">
                    <div class = "question_box">
                        <input type="text" name= "question_<?= $question['question']['question_id'] ?>" value="<?= $question['question']['string'] ?>" required="required">
                    </div>
                    <?php
                    foreach ($question['answers'] as $answer) {
                        /*var_dump($answer);*/
                        ?>
                            <div class="answer">
                                <input type="text" name="answer_<?= $answer['answer_id']?>"
                                       value="<?= $answer['string'] ?>" required="required">
                            </div>
                        
                    <?php } ?>
                    <input type="button" value="Add Answer"/>
                </div>
            <?php } ?>
            <input type="button" value="Add Question"/>
            <div>
                <input type="hidden" value="<?=$_GET['id']?>" name="poll_id" />
            </div>
            <input class="button" type="submit">

        </form>
    <?php
    }
    else
    {
    ?>
        <form name = 'addQuestions' action="addQuestionsAction.php" method="post">
            <div class="question">
                <div class="question_div">
                    <input type="text" name="question_1">
                </div>
                <div class="answer">
                    <input type="text" name="answer_1.1">
                </div>
                <div class="answer">
                    <input type="text" name="answer_1.2">
                </div>
                <input type="button" value="Add Answer"/>
            </div>
            <input type="button" value="Add Question"/>
            <div>
                <input type="hidden" value="<?=$_GET['id']?>" name="poll_id" />
            </div>
            <input class="submit" type="submit" />
        </form>
    <?php
    }
    ?>

    <footer><h3></h3></footer>
</section>
</body>
</html>