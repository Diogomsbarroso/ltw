<?php
/**
 * Created by PhpStorm.
 * User: João
 * Date: 02/12/2014
 * Time: 17:12
 */

include_once('../init.php');
include_once($_BASE_DIR . 'templates/header.php');
?>
    <script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="<?=$_BASE_URL?>jquery/search.js"></script>

    <form>
        <small>Search for polls: </small>
        <input id="search_input" placeholder="Search">
    </form>

    <section id="poll_list">

    </section>


<?php
include_once($_BASE_DIR . 'templates/footer.php');
?>