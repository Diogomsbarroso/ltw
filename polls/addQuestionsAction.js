window.addEventListener("load",loadDocument,true);

var QUESTION_ITERATOR = 1;
var ANSWER_ITERATOR = 1;

function loadDocument()
{
    var form = document.forms['addQuestions'];
    var question = document.forms['addQuestions'];
    question = question.firstChild;
    question = question.nextSibling;
    var answer_button = firstNamedChild(question, "INPUT");
    firstNamedChild(form, "INPUT").addEventListener("click",addQuestion,true);
    firstNamedChild(question, "INPUT").addEventListener("click",addAnswer.bind(null, question), true);
}

function prepareAnswer(father)
{
    ANSWER_ITERATOR += 1;
    var newNode = document.createElement('input');
    var newDiv = document.createElement('div');
    newNode.type="text";
    newNode.name = "answer_" + QUESTION_ITERATOR + "." + ANSWER_ITERATOR;
    newDiv.appendChild(newNode);

    return newDiv;
}

function prepareQuestionBox()
{
    var newNode = document.createElement('input');
    var newDiv = document.createElement('div');
    newNode.type="text";
    newNode.name = "question_" + QUESTION_ITERATOR;
    ANSWER_ITERATOR += 1;
    newDiv.appendChild(newNode);

    return newDiv;
}

function prepareQuestion()
{
    QUESTION_ITERATOR += 1;
    var new_Question = document.createElement('div');
    var answer_1 = prepareAnswer();
    var answer_2 = prepareAnswer();
    var question_1 = prepareQuestionBox();
    var new_button = document.createElement('input');
    new_button.type = "button";
    new_button.value = "Add Answer";
    new_Question.appendChild(question_1);
    new_Question.appendChild(answer_1);
    new_Question.appendChild(answer_2);
    new_Question.appendChild(new_button);

    return new_Question;
}

function firstTagChild(element)
{
    var firstChild = element.firstChild;

    while(firstChild.nodeType != 1)
        firstChild = firstChild.nextSibling;

    return firstChild;
}

function firstNamedChild(el, tagName)
{
    var first_child = el.firstChild;

    while(first_child.tagName != tagName)
    {
        first_child = first_child.nextSibling;
    }

    return first_child;
}

function addAnswer(father)
{
    var element = father;
    father.insertBefore(prepareAnswer(), firstNamedChild(father,"INPUT"));
}

function addQuestion()
{
    var element = document.forms['addQuestions'];

    var newDiv = prepareQuestion(element);
    firstNamedChild(newDiv, "INPUT").addEventListener("click",addAnswer.bind(null, newDiv), true);
    element.insertBefore(newDiv,firstNamedChild(element,"INPUT"));
}