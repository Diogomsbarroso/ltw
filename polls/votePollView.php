<?php
include_once('../init.php');
include_once($_BASE_DIR . 'templates/header.php');
?>

<?php
if ($dataBase->hasUserVoted($_GET['id'],$_SESSION['userID']))
{

    header("Location: viewPollView.php?id=" . $_GET['id']);
    exit();

}
$poll = $dataBase->showPoll($_GET['id']);

$questions = $dataBase->showQuestions($_GET['id']);
?>

<h1>
    <?= $poll[0]['string'] ?>
</h1>

<form action="voteAction.php" method="post">
    <?php
    foreach ($questions as $question) {
        /*var_dump($question);*/
        ?>

        <h2><?= $question['question']['string'] ?> </h2>

        <?php
        foreach ($question['answers'] as $answer) {
            /*var_dump($answer);*/
            ?>
            <label>
                <section class="box">
                    <input type="radio" name="<?=$question['question']['question_id']?>" value="<?=$answer['answer_id']?>" required="required">
                    <?=$answer['string']?>
                </section>
            </label>
        <?php } ?>
    <?php } ?>
    <input class="button" type="submit">

</form>

<?php
include_once($_BASE_DIR . 'templates/footer.php');
?>