<?php
function getPostVariable($variableName)
{
    if(isset($_POST[$variableName]))
        return $_POST[$variableName];

    return "";
}

function requireSSL()
{
    if($_SERVER["HTTPS"] != "on")
    {
        header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
        exit();
    }
}

function isLoggedIn()
{
    return isset($_SESSION["userID"]);
}

function in_array_r($needle, $haystack, $strict = false) {
    foreach ($haystack as $item) {
        if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
            return true;
        }
    }

    return false;
}
?>