<?php
/**
 * Created by PhpStorm.
 * User: João
 * Date: 05/12/2014
 * Time: 10:55
 */
include_once('../init.php');
include_once($_BASE_DIR . 'templates/header.php');
?>


    <!-- Username*, FirstName, LastName, BirthDate*, Email*, Password* -->
    <form id="register" action="registerAction.php" method="POST">
        <input type="text" name="username" placeholder="Username" required="required">
        <input type="text" name="firstName" placeholder="First Name">
        <input type="text" name="lastName" placeholder="Last Name">
        <input type="date" name="birthDate" placeholder="Birth Date" required="required">
        <input type="email" name="email" placeholder="E-Mail" required="required">
        <input type="password" name="password" placeholder="Password" required="required">
        <input type="password" name="confirmPassword" placeholder="Confirm Password" required="required">
        <input class="button" type="submit" name="Submit">
    </form>




<?php
include_once($_BASE_DIR . 'templates/footer.php');
?>