
<?php
include_once('../init.php');
include_once($_BASE_DIR . 'templates/header.php');
?>

<div class="box">
    <!-- Username, Password -->
    <form action="loginAction.php" method="POST">
        <input type="text" name="username" required="required" placeholder="Username">
        <input type="password" name="password" required="required" placeholder="Password">
        <input class="button" type="submit" name="Log In" value="Log In">
    </form>
</div>

<?php
include_once($_BASE_DIR . 'templates/footer.php');
?>