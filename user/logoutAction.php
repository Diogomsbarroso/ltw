<?php
include_once("../init.php");

session_unset();
session_destroy();

header("location: " . $_BASE_URL ."");

exit();
?>