<?php
include_once("../init.php");

// Retrieve post data:
$username = getPostVariable("username");
$password = getPostVariable("password");

// Set default next page:
$errorPage = $_SERVER['HTTP_REFERER'];
$nextPage = $errorPage;
unset($message);

// If fields are set:
if($username !== "" && $password !== "")
{
    // If user exists:
    if($dataBase->verifyUsernameExistence($username) === true)
    {
        // If password is correct:
        if($dataBase->verifyUserPassword($username, $password) === true)
        {
            // Set session userID and username:
            $_SESSION["userID"] = $dataBase->getUserID($username);
            $_SESSION["username"] = $username;

            // Go to home:
        }
        // If password is incorrect:
        else
            $message = "Wrong password!";
    }
    // If user doesn't exist:
    else
        $message = "That user does not exist!";
}
// If fields are empty or not set:
else
    $message = "Username and/or Password fields are not set!";

$_SESSION["message"] = $message;

// Redirect to next page:

header("Location: " . $nextPage);
exit();

?>