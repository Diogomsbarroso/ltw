<?php
	include_once("../init.php");

	// Retrieve post data:
	$username = getPostVariable("username");
	$firstName = getPostVariable("firstName");
	$lastName = getPostVariable("lastName");
	$birthDate = getPostVariable("birthDate");
	$email = getPostVariable("email");
	$password = getPostVariable("password");
	$confirmPassword = getPostVariable("confirmPassword");

	$errorPage = $_SERVER['HTTP_REFERER'];
	$nextPage = $errorPage;

	var_dump($password);
	var_dump($confirmPassword);

	// If required fields are not set:
	if($username === "" || $birthDate === "" || $email === "" || $password === "")
	{
		// Go to user:
		$message = "Required fields not set!";
	}
	// If password fields do not match:
	else if($password !== $confirmPassword)
	{
		// Go to user:
		$message = "Password fields do not match!";
	}
	else
	{
		// If user and email don't exist:
		if($dataBase->verifyUsernameExistence($username) !== true && $dataBase->verifyEmailExistence($email) !== true)
		{
			// Add new user to database:
			$dataBase->addUser($username, $firstName, $lastName, $birthDate, $email, $password);

			$nextPage = $_BASE_URL;
            $message = "Your registration was successful! Please log in with your credentials.";
		}
		// If user already exists:
		else
		{
			// Go to user:
			$message = "Username or e-mail already in use!";
		}
	}

	$_SESSION["message"] = $message;

	// Redirect to next page:
	header("Location: " . $nextPage);
?>