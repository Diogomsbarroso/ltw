<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>

<?php if(isLoggedIn()) { ?>

    <div id="navigationBar">
        <a href="<?=$_BASE_URL?>">Home</a>
        <a href="<?=$_BASE_URL . "polls/createPollView.php" ?>">Create Poll</a>
        <a href="<?=$_BASE_URL . "polls/viewOwnPollsView.php" ?>">Manage Polls</a>
        <a href="<?=$_BASE_URL . "user/logoutAction.php" ?>">Log out</a>
        <div id="welcome">Welcome <?= $dataBase->getUsername($_SESSION['userID']) ?></div>
    </div>
<?php } else { ?>
    <script src="<?=$_BASE_URL?>templates/navigationBar.js"></script>

    <div id="navigationBar">
        <a href="<?=$_BASE_URL?>">Home</a>
        <a id="login">Log In</a>
        <a href="<?=$_BASE_URL . "user/registerView.php" ?>">Register</a>
        <?php
        if (isset($_SESSION['message'])) { ?>
            <span class="error"><?=$_SESSION['message']?></span>
        <?php
            unset($_SESSION['message']);
        } ?>

        <div id="loginDiv" style="height: 0; overflow: hidden">


            <form action="<?=$_BASE_URL?>user/loginAction.php" method="POST">
                <input type="text" name="username" required="required" placeholder="Username">
                <input type="password" name="password" required="required" placeholder="Password">
                <input class="button" type="submit" name="Log In" value="Log In">
            </form>
        </div>
    </div>
<?php } ?>